import RPi.GPIO as GPIO    # Import Raspberry Pi GPIO library
from time import sleep     # Import the sleep function from the time module
GPIO.setwarnings(False)    # Ignore warning for now
GPIO.setmode(GPIO.BOARD)   # Use physical pin numbering
GPIO.setup(8, GPIO.OUT, initial=GPIO.LOW)   # Set pin 8 to be an output pin and set initial value to low (off)

while True:

        # Reading and storing the data coming from Arduino
        data = input("enter 1 to turn on 1 to turn off led")

        # Light up or light down the led depending on the incoming data
        if data == '1':
            GPIO.output(8, 1)
        elif data == '0':
            GPIO.output(8, 0)
